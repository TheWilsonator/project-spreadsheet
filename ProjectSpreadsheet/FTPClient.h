//Includes
#include <iostream>
#include <boost/asio.hpp>
#include <string>
using namespace std;
using namespace boost::asio::ip;
class FTPClient{
private:
	string Host;
	int Socket;
	string User;
	string Pass;
	string DATA;
	//List of command to be able to check if commands are valid
	tcp::iostream request_stream;
	int PassivePort = 0;
	int ActivePort = 0;
	string RESPONSE= "";
	string Error;
	bool Connected = false;
	string DATA_Host;
	int DATA_Port;
	tcp::iostream Data_stream;
public:
	FTPClient(string host,int port){
		Host = host;
		Socket = port;
	}
	bool Connect(string user,string pass){
		//request_stream.connect(Host,Socket);
		if(!request_stream){
			return false;
		}
		request_stream << "USER " << User;
		request_stream.flush();
		cout << RESPONSE;
		getline(request_stream,RESPONSE);
		if(RESPONSE.find("331")!= -1){
		//User Exists
		request_stream << "PASS " << Pass;
		request_stream.flush();
			if(RESPONSE.find("230")!= -1){
				//Pass Exists
				RESPONSE = "";
				Connected = true;
				return true;
			}
			else{
			RESPONSE = "Password Not Valid";
			return false;
			}
		}
		else{
			RESPONSE = "Username Not Valid";
			return false;
		}
	}
	bool Disconnect(){
		try{
			request_stream.close();
			return true;
		}
		catch(string Ex){
			Error = Ex;
			return false;
		}
	}
	bool IsConnected(){
		return Connected;
	}
	bool GetDataPassive(){
		try{
			string Data;
			Data_stream.connect(DATA_Host,DATA_Port);
			std::getline(request_stream,Data);
			DATA = Data;
			return true;
		}
		catch(int x){return false;}
	}
	bool GetDataActive(){
		try{
			string Data;

			DATA = Data;
			return true;
		}
		catch(exception ex){
			//Error = ex;
			return false;
		}
	}
	bool IsPassive(){
		if(PassivePort!= -1){
			return true;
		}
		else{
			return false;
		}
	}
	bool IsActive(){
		if(ActivePort!= -1){
			return true;
		}
		else{
			return false;
		}
	}
	int GetPassivePort(){
		return PassivePort;
	}
	int GetActivePort(){
		return ActivePort;
	}
	bool SetPassive(){
		try{
			int Host_1;
			int Host_2;
			string PassiveRAW;
			string portstring;
			request_stream << "PASV";
			std::getline(request_stream,PassiveRAW);
			if(PassiveRAW.find("227")!= -1){
				PassiveRAW.erase(0,PassiveRAW.find("(")+1);
				PassiveRAW.erase(PassiveRAW.find(")")+1,PassiveRAW.length()-PassiveRAW.find(")"));
				int temp;
				while(temp<=3){
				PassiveRAW.replace(PassiveRAW.find(","),1,".");
				temp++;
				}
				string TEMP1 = PassiveRAW;
				string TEMP2 = PassiveRAW;
				TEMP1.erase(TEMP1.find(","),TEMP1.length()- TEMP1.find(","));
				DATA_Host = TEMP1;
				TEMP2.erase(0, TEMP2.find(",")+1);
				TEMP2.erase(0,TEMP2.find(",")+1);
				TEMP2.erase(TEMP2.find(","),TEMP2.length()-TEMP2.find(","));
				Host_1 = atoi(TEMP2.c_str());
				TEMP2 = PassiveRAW;
				TEMP2.erase(0,TEMP2.find(",")+1);
				TEMP2.erase(0,TEMP2.find(",")+1);
				Host_2 = atoi(TEMP2.c_str());
				PassivePort = Host_1 *256 + Host_2;
				return true;
			}
			else{
				Error = "FTP Server Failed Passive Mode";
			}
		}
		catch(exception ex){
			//Error = ex.to_string();
			return false;
		}
	}
	bool SetActive(){
		return false;
	}
	bool SendRAW(string data){
		try{
			if(Connected){
				request_stream << data;
				std::getline(request_stream,RESPONSE);
				return true;
			}
			else{
				Error = "FTP Client Not Connected";
				return false;
			}
		}
		catch(exception ex){
			//Error = ex.to_string();
			return false;
		}
	}
	string GetResponse(){
		return RESPONSE;
	}
	bool SendCommand(string Command){
		//validate command
		//if(IsInVector(Command)){
			SendRAW(Command);
		//}
	}
};
